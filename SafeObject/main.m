//
//  main.m
//  SafeObject
//
//  Created by chenleping on 2018/10/26.
//  Copyright © 2018年 DSY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
