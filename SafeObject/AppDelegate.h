//
//  AppDelegate.h
//  SafeObject
//
//  Created by chenleping on 2018/10/26.
//  Copyright © 2018年 DSY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

